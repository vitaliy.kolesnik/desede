package by.softclub.crypt.desede.controller;

import by.softclub.crypt.desede.domain.RequestDto;
import by.softclub.crypt.desede.service.CryptoService;
import by.softclub.crypt.desede.util.CryptoUtils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping()
public class CryptoController {

    private final CryptoService cryptoService;

    public CryptoController(CryptoService cryptoService) {
        this.cryptoService = cryptoService;
    }

    @PostMapping(value = "/encrypt")
    public ResponseEntity<String> encrypt(@RequestBody RequestDto dto) {
        try {
            return new ResponseEntity<>(cryptoService.encrypt(dto), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/decrypt")
    public ResponseEntity<String> decrypt(@RequestBody RequestDto dto) {
        try {
            return new ResponseEntity<>(cryptoService.decrypt(dto), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(value = "/decodeHex")
    public ResponseEntity<String> decodeHex(@RequestBody RequestDto dto) {
        try {
            return new ResponseEntity<>(cryptoService.decodeHex(dto), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        }
    }


}
