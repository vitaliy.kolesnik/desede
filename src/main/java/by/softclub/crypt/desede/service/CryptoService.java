package by.softclub.crypt.desede.service;

import by.softclub.crypt.desede.domain.RequestDto;

public interface CryptoService {

    String encrypt(RequestDto dto);

    String decrypt(RequestDto dto);

    String decodeHex(RequestDto dto);
}
