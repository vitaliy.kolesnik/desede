package by.softclub.crypt.desede.service.impl;

import by.softclub.crypt.desede.domain.RequestDto;
import by.softclub.crypt.desede.service.CryptoService;
import by.softclub.crypt.desede.util.CryptoUtils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CryptoServiceImpl implements CryptoService {

    @Value("${encrypt.key}")
    private String key;

    @Value("${encrypt.iv}")
    private String iv;

    @Value("${encrypt.base64}")
    private boolean isBase64;

    @Value("${decrypt.key}")
    private String decryptKey;

    @Value("${decrypt.iv}")
    private String decryptIv;

    @Value("${decrypt.base64}")
    private boolean decryptIsBase64;

    @Override
    public String encrypt(RequestDto dto) {
        try {
            return CryptoUtils.encrypt(dto.getData(), key, iv, isBase64);
        } catch (Exception e) {
            throw new RuntimeException("Error encryption data: " + e.getLocalizedMessage());
        }
    }

    @Override
    public String decrypt(RequestDto dto) {
        try {
            return CryptoUtils.decrypt(dto.getData(), decryptKey, decryptIv, decryptIsBase64);
        } catch (Exception e) {
            throw new RuntimeException("Error encryption data: " + e.getLocalizedMessage());
        }
    }

    @Override
    public String decodeHex(RequestDto dto) {
        try {
            byte[] bytes = CryptoUtils.decodeHex(dto.getData().toCharArray());
            return new String(bytes);
        } catch (Exception e) {
            throw new RuntimeException("Error encryption data: " + e.getLocalizedMessage());
        }
    }

}
