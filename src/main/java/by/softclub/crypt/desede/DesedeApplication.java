package by.softclub.crypt.desede;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@PropertySources({
        @PropertySource(name = "default", value = "classpath:application.properties"),
        @PropertySource(name = "external", value = "file:${custom.properties:}", ignoreResourceNotFound = true)
})
public class DesedeApplication {

    public static void main(String[] args) {
        SpringApplication.run(DesedeApplication.class, args);
    }

}
