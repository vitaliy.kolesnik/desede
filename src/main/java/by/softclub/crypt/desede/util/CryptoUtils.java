package by.softclub.crypt.desede.util;

import javax.crypto.Cipher;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Base64;

public class CryptoUtils {

    public static final String DESEDE_CBC_PKCS5PADDING = "desede/CBC/PKCS5Padding";
    public static final String ALGORITHM = "DESede";


    public static String encrypt(String message, String key, String ivStr, boolean isBase64) throws Exception {
        return crypto(message, key, ivStr, isBase64, Cipher.ENCRYPT_MODE);
    }

    public static String decrypt(String message, String key, String ivStr, boolean isBase64) throws Exception {
        return crypto(message, key, ivStr, isBase64, Cipher.DECRYPT_MODE);
    }

    private static String crypto(String message, String key, String ivStr, boolean isBase64, int mode) throws Exception {
        final SecretKeySpec desKey = new SecretKeySpec(decodeHex(key.toCharArray()), ALGORITHM);
        final Cipher cipher = Cipher.getInstance(DESEDE_CBC_PKCS5PADDING);
        final IvParameterSpec iv = new IvParameterSpec(ivStr.getBytes());
        cipher.init(mode, desKey, iv);

        byte[] decodedBytes;
        if (isBase64) {
            decodedBytes = Base64.getDecoder().decode(message);
        } else {
            decodedBytes = message.getBytes();
        }
        byte[] encrypted = cipher.doFinal(decodedBytes);
        if (mode == Cipher.ENCRYPT_MODE) {
            return Base64.getEncoder().encodeToString(encrypted);
        } else if (mode == Cipher.DECRYPT_MODE) {
            return new String(encrypted);
        }
        return "";
    }

    public static byte[] decodeHex(char[] data) throws Exception {

        int len = data.length;
        if ((len & 0x01) != 0) {
            throw new Exception("Odd number of characters.");
        }
        byte[] out = new byte[len >> 1];
        // two characters form the hex value.
        for (int i = 0, j = 0; j < len; i++) {
            int f = toDigit(data[j], j) << 4;
            j++;
            f = f | toDigit(data[j], j);
            j++;
            out[i] = (byte) (f & 0xFF);
        }
        return out;
    }

    public static char[] encodeHex(byte[] data) {
        final char[] DIGITS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

        int l = data.length;
        char[] out = new char[l << 1];
        // two characters form the hex value.
        for (int i = 0, j = 0; i < l; i++) {
            out[j++] = DIGITS[(0xF0 & data[i]) >>> 4];
            out[j++] = DIGITS[0x0F & data[i]];
        }
        return out;
    }

    private static int toDigit(char ch, int index) throws Exception {
        int digit = Character.digit(ch, 16);
        if (digit == -1) {
            throw new Exception("Illegal hexadecimal character " + ch + " at index " + index);
        }
        return digit;
    }

}
