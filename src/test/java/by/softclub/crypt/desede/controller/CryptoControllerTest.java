package by.softclub.crypt.desede.controller;

import by.softclub.crypt.desede.domain.RequestDto;
import by.softclub.crypt.desede.util.CryptoUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CryptoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void encryptTest() throws Exception {
        final String expect = "Dy5qUXP5SGuZ+mWgxGuMc8ujvI+Zeo74LEr4mkmaMpRACFWL2FM84DJmOmr+vv0vHwinfZglxFyUfB95f91QDTQ212w6+UnbhW8WFHW0gcLRqFyPQXk1b06OkCvexoK/B/3S3/LNHhE=";

        var dto = new RequestDto();

//        dto.setData("{\"terminalId\":\"QR_SY\",\"scanData\":\"#BSQR#0#b58638fc39ddcab4by.besmart.bstokendemo:4#327954:960757\"}");
        dto.setData("qwerty");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/encrypt")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        String resultStr = result.getResponse().getContentAsString();
        assertNotNull(resultStr);
        assertEquals(expect, resultStr);
        System.out.println(resultStr);

    }

    @Test
    void decryptTest() throws Exception {
        final String expect = "{\"terminalId\":\"QR_SY\",\"scanData\":\"#BSQR#0#b58638fc39ddcab4by.besmart.bstokendemo:4#327954:960757\"}";

        var dto = new RequestDto();

        dto.setData("Dy5qUXP5SGuZ+mWgxGuMc8ujvI+Zeo74LEr4mkmaMpRACFWL2FM84DJmOmr+vv0vHwinfZglxFyUfB95f91QDTQ212w6+UnbhW8WFHW0gcLRqFyPQXk1b06OkCvexoK/B/3S3/LNHhE=");

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.post("/decrypt")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(dto))
                .characterEncoding("UTF-8")
                .locale(new Locale("ru", "RU"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andReturn();

        String resultStr = result.getResponse().getContentAsString();
        assertNotNull(resultStr);
        assertEquals(expect, resultStr);
        System.out.println(resultStr);

    }

    @Test
    void decodeBase64Test() {
        String str = "eyJ0ZXJtaW5hbElkIjoiUVJfU1kiLCJzY2FuRGF0YSI6IiNCU1FSIzAjYjU4NjM4ZmMzOWRkY2FiNGJ5LmJlc21hcnQuYnN0b2tlbmRlbW86NCMzMjc5NTQ6OTYwNzU3In0";
        byte[] resultBytes = Base64.getDecoder().decode(str);
        String result = new String(resultBytes);
        System.out.println(result);
        assertNotNull(result);
    }

}
